<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

	<?php if ($model->isNewRecord || \Yii::$app->user->can('updatePassword') || \Yii::$app->user->can('updateOwnPassword', ['user' =>$model])): ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
	<?php endif; ?>
	<?php /* $model->isNewRecord    checking if we are creating a new user */ ?>
	<?php /* \Yii::$app->user->can('updatePassword')   checking if the admin is logedIn  */ ?>
	<?php /* \Yii::$app->user->can('updateOwnPassword', ['user' =>$model])    we want the user to be able to change his own password, we use , ['user' =>$model], 
		to map and check if this is the loged in user. we don't need that in the admin because the admin can change the password for all users.*/ ?>
    
	<?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

	<?php if (\Yii::$app->user->can('createUser')) { ?>
		<?= $form->field($model, 'role')->dropDownList($roles) ?>		
	<?php } ?>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php //var_dump(User::getRoles())  ?>	

</div>
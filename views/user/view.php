<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

/* changed the header of the view*/
$this->title = $model->username; /*$model->id - id was in the original code*/
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (\Yii::$app->user->can('createUser')) { ?>
    <p>

		<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>	
    </p>
	<?php } ?>	
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            /*'password',
            'auth_key',*/
            'firstname',
            'lastname',
            'email:email',
            'phone',
			//user role
			[ // The role of the user
				'label' => $model->attributeLabels()['role'],
				'value' => $model->userole,
			],
            //'created_by',
			[ // User created by
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->fullname) ? $model->createdBy->fullname : 'No one!',	
			],			
            //'created_at',
			[ // user created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],	
            //'updated_by',
			[ // User updated by
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->fullname) ? $model->updateddBy->fullname : 'No one!',	
			],
			//'updated_at',
			[ // User updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],
        ],
    ]) ?>
	
	<?php /*var_dump(Yii::$app->authManager->getRolesByUser($model->id)) */?>

</div>
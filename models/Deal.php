<?php

namespace app\models;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadid
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leadid', 'name', 'amount'], 'required'],
            [['leadid', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }
	
		public function getLeadItem()
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadid']);
    }
	
		public function getFullname()
    {
		/*$allDeals = self::find()->all();
		$allDealsArray = ArrayHelper::
					map($allDeals, 'id', 'name');
		return $allDealsArray;	*/
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadid' => 'Lead',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
}
